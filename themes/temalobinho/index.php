        <?php get_header(); ?>
        <main>
            <section class="adote">
                <figure>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/adote4.jpg" alt="Imagem Lobo" class="imgAdote">
                </figure>
                <div>
                    <h1><?php the_field('titulo_inicial') ?></h1>
                    <hr>
                    <p><?php the_field('descricao_inicial') ?></p>
                </div>
            </section>
            <section class="sobre">
                <h2>Sobre</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eos ratione quam ex provident quia aliquam ullam corporis placeat maiores id cum ab debitis, ipsa, illo praesentium. Quae porro reprehenderit recusandae. Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto adipisci modi, officia amet commodi dolores accusantium, neque quae quaerat quasi nemo. Quas veniam qui harum optio, ad doloremque illo praesentium!</p>
            </section>
            <section class="valores">
                <h2>Valores</h2>
                <div class="cardsDispFlex"> 
                    <div class="cardValores">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-protecao.png" alt="">
                        <h4>Proteção</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium vero recusandae molestias cum aperiam temporibus sunt expedita illo cumque. Voluptatum ipsum vero harum ratione libero ut inventore dolorum quidem dolores.</p>
                    </div>
                    <div class="cardValores">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-carinho.png" alt="">
                        <h4>Carinho</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium vero recusandae molestias cum aperiam temporibus sunt expedita illo cumque. Voluptatum ipsum vero harum ratione libero ut inventore dolorum quidem dolores.</p>
                    </div>
                    <div class="cardValores">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-companheirismo.png" alt="">
                        <h4>Companheirismo</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium vero recusandae molestias cum aperiam temporibus sunt expedita illo cumque. Voluptatum ipsum vero harum ratione libero ut inventore dolorum quidem dolores.</p>
                    </div>
                    <div class="cardValores">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-resgate.png" alt="">
                        <h4>Resgate</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium vero recusandae molestias cum aperiam temporibus sunt expedita illo cumque. Voluptatum ipsum vero harum ratione libero ut inventore dolorum quidem dolores.</p>
                    </div>
                </div>
            </section>

            <section class="exemplo">
                <h2>Lobos Exemplo</h2>

                <div class="dispExImpar">
                    <div class="quadroAzulImpar">
                        <!-- <img src="home-page/lobo-exemplo1.png" alt="" class="imgExemploImpar"> -->

                    </div>
                    <div class="txtExemplo">
                        <h3 class="nomeLoboImpar">Nome do Lobo</h3>
                        <h5 class="idadeLoboImpar">Idade: XX anos</h5>
                        <p class="descrLoboImpar">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laboriosam debitis a saepe inventore animi blanditiis quaerat aspernatur placeat porro velit. Odio eum ea quia. Nam consequatur nemo vel nostrum repellat?</p>
                    </div>
                </div>

                <div class="dispExPar">
                    <div class="txtExemplo">
                        <h3 class="nomeLoboPar">Nome do Lobo</h3>
                        <h5 class="idadeLoboPar">Idade: XX anos</h5>
                        <p class="descrLoboPar">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laboriosam debitis a saepe inventore animi blanditiis quaerat aspernatur placeat porro velit. Odio eum ea quia. Nam consequatur nemo vel nostrum repellat?</p>
                    </div>
                    <div class="quadroAzulPar">
                    </div>
                </div>
            </section>
        </main>
        <?php get_footer(); ?>
