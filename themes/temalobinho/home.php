<?php
// Template Name: página dos lobinhos
?>

<?php get_header(); ?>
    <main class="dispFlexMain">


        <section class="areaBusca">
            <div class="busca">
                <button class="btnBusca" onclick="buscar()"><img src="lista-de-lobinhos/search.png"></button>
                <input type="text" id="barraBusca">
                <a href="AddLobinho.HTML"><input type="button" class="addLobo" value="+ Lobo"></a>
            </div>
            <label class="adotados">
                <input type="checkbox" id="checkbox"> Ver lobinhos adotados
            </label>
        </section>

        <?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>
        <?php $numero = 0 ?>
        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
        <?php if($numero % 2 == 0){ ?>

        <div class="dispExImpar">
            <div class="quadroAzulImpar">
                <?php if(get_field('lobo_foto')): ?>
                    <img src="<?php the_field('lobo_foto'); ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="txtExemplo">
                <h3 class="nomeLoboImpar">Nome do Lobo: <?php the_field('lobo_titulo') ?></h3>
                <h5 class="idadeLoboImpar">Idade: <?php the_field('lobo_idade') ?> anos</h5>
                <p class="descrLoboImpar"><?php the_field('lobo_descricao') ?></p>
            </div>
        </div>

        <?php }
        else{ ?>

        <div class="dispExPar">
            <div class="txtExemplo">
            <h3 class="nomeLoboImpar">Nome do Lobo: <?php the_field('lobo_titulo') ?></h3>
                <h5 class="idadeLoboImpar">Idade: <?php the_field('lobo_idade') ?> anos</h5>
                <p class="descrLoboImpar"><?php the_field('lobo_descricao') ?></p>
            </div>
            <div class="quadroAzulPar">
                <?php if(get_field('lobo_foto')): ?>
                    <img src="<?php the_field('lobo_foto'); ?>" alt="">
                <?php endif; ?>
            </div>
        </div>

        <?php } $numero++; ?>
        <?php endwhile; wp_reset_postdata(); ?>
            
        <div class="paginacao">
            <?php my_pagination(); ?>
        </div>
        
    </main>
<?php get_footer(); ?>