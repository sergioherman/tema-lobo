<?php
// Template Name: pagina quem somos
?>

<?php get_header(); ?>

    <main>

        <div>
            <div class="titulo_quem_somos"> <h3><?php the_title() ?> </h3></div>
            <div class="descricao_quem_somos"><?php the_content() ?></div>
        </div>

    </main>

<?php get_footer(); ?>