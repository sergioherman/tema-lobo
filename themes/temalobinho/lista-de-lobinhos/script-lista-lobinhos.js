const url = `https://lobinhos.herokuapp.com/wolves/`
const urlAdopted = `https://lobinhos.herokuapp.com/wolves/adopted`
const checkbox = document.getElementById('checkbox')
const containerMensage = document.querySelector(".containerLobos")
const barraBusca = document.querySelector("#barraBusca")
const btnBusca = document.querySelector(".btnBusca")
let fetchConfig = {
    method: "GET"
}

function createInfos(id, name, age, description, image_url, adopted, adopter_name){
    var infoLobos = document.createElement("div")
    infoLobos.classList.add("infoLobos")
    infoLobos.setAttribute("id", id)
    infoLobos.setAttribute("adopted", adopted)
    infoLobos.setAttribute("name", name)
  
    let txtLobos = document.createElement("div")
    txtLobos.classList.add("txtLobos") 
    
    let nameLobo = document.createElement("div")
    nameLobo.classList.add("nameLobo")
    nameLobo.innerHTML = `<span>${name}</span>`

    let ageLobo = document.createElement("span")
    ageLobo.classList.add("ageLobo")
    ageLobo.innerText = `Idade: ${age} anos`
    
    let descriptionLobo = document.createElement("span")
    descriptionLobo.classList.add("descriptionLobo")
    descriptionLobo.innerText = `${description}`

    let imgLobo = document.createElement("img")
    imgLobo.classList.add("img")
    imgLobo.setAttribute('src', image_url);

    if (adopted == false) {
        var botaoAdotar = document.createElement("input")
        botaoAdotar.classList.add("botaoAdotar")
        botaoAdotar.setAttribute("type", "button")
        botaoAdotar.setAttribute("value", "Adotar")
        botaoAdotar.setAttribute("onclick", "adotar(this)")
        botaoAdotar.setAttribute("id", id)

        botaoAdotar.innerHTML
        nameLobo.appendChild(botaoAdotar)

    }else{
        var loboAdotado = document.createElement("div")
        loboAdotado.classList.add("loboAdotado")
        loboAdotado.innerHTML = 'Adotado'
        nameLobo.appendChild(loboAdotado)

        let adopter_nameLobo = document.createElement("span")
        adopter_nameLobo.classList.add("adopter_nameLobo")
        adopter_nameLobo.innerText = `Adotado por: ${adopter_name}`
        txtLobos.appendChild(adopter_nameLobo)
    }

    txtLobos.appendChild(nameLobo)
    txtLobos.appendChild(ageLobo)
    txtLobos.appendChild(descriptionLobo)
    
    infoLobos.appendChild(txtLobos)
    infoLobos.appendChild(imgLobo)

    containerMensage.append(infoLobos)
}

function getApi(url){
    fetch(url, fetchConfig)
        .then(response => response.json()
        .then(resp=> {resp.forEach(lobo => {
            createInfos(lobo.id, lobo.name, lobo.age, lobo.description, lobo.image_url, lobo.adopted, lobo.adopter_name)
            })})
            .catch(error => {console.log(error)})
        )
        .catch(error => {console.log(error)})
}


function adotar (btn){
    window.location.href = `show-lobinho.html?lobo=${btn.id}`
}

checkbox.addEventListener("click", function(e) {
    if (checkbox.checked) {
        containerMensage.innerHTML = ""
        getApi(urlAdopted, fetchConfig)
    } else {
        containerMensage.innerHTML = ""
        getApi(url, fetchConfig)
    }
})


getApi(url)